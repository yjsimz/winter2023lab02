public class Calculator 
{
	public int add(int num1, int num2)
	{
		int sum = num1 + num2;
		return sum;
	}
	public int subtract(int num1, int num2)
	{
		int diff = num1 - num2;
		if (diff>0)
		{
			return diff;
		}
		else 
		{
			return diff*-1;
		}
	}
	public static int multiply (int num1, int num2)
	{
		int mult = num1 * num2;
		return mult;
	}
	public static int divide (int num1, int num2)
	{
		int div = num1/num2;
		return div;
	}
}