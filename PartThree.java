import java.util.Scanner;

public class PartThree
{
	public static void main (String[] args)
	{
		Scanner scan = new Scanner(System.in);
		
		Calculator calc = new Calculator();
		
		System.out.print ("Please enter the first number:");
		int num1 = scan.nextInt();
		System.out.print ("Please enter the second number:");
		int num2 = scan.nextInt();
		
		System.out.println ("Sum of the first number and the second number is: " + calc.add(num1, num2));
		System.out.println ("Difference of the first number and the second number is: " + calc.subtract(num1, num2));
		System.out.println ("Multiplacation of the first number and the second number is: " + Calculator.multiply(num1, num2));
		System.out.println ("Quotient of the first number and the second number is: " + Calculator.divide(num1, num2));
	}
}